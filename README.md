<table align="center"><tr><td align="center" width="9999">
<img src="https://www.freeiconspng.com/uploads/live-chat-icon-6.jpg" align="center" width="150" alt="Project icon">

# CPP Lesson 13 - MT Server & client Chat

Homeworks for lesson 13 by Gabriel Bialyk
</td></tr></table>


## Table of contents

- [CPP Lesson 13 - Chat](#cpp-lesson-13---mt-server-&-client-chat)
	- [Table of contents](#table-of-contents)
	- [Quick start](#quick-start)
	- [Status](#status)
	- [What's included](#whats-included)
	- [Creators](#creators)
	- [Credit](#credit)

## Quick start

- Open "MyChat" folder and download the exe files.
- To run the server with VS make sure that the config.txt is in the sln files.

## Status

- Loggin GUI with Username&Pass and show your name at top left.

## What's included

```text
.CPP, .H, .Sln (for VisualStudio), .EXE & .txt
```

## Creator

**G.B**

- <https://gitlab.com/Toxey>

## Credit

**M.B**  - help me to do stuff.

- <https://gitlab.com/TheNcar>